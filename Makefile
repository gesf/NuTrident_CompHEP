#
# Makefile for NuTrident_CompHEP
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Sep 29 14:27:01 CEST 2015
#

scripts = run.sh scan.sh split.sh group.sh prepare.sh count.sh cuts.gp kill.sh sm.sh oneclickcode.sh kin.sh reg.sh wait.sh Zprime.sh Sprime.sh
myOwnFiles = working/usr/userFun.c working/usr/userFun.h working/models/*1?.mdl

scp2:
	scp $(scripts) gesf@heget.kek.jp:~/nuTrident_myOwn/

record:
	uname -a > record.log
	echo >> record.log
	echo $(PWD) >> record.log
	echo >> record.log
	date >> record.log

# backup [time stamp according to the newest file]
backupFiles= $(scripts) $(myOwnFiles) *.tab ChangeLog Makefile README
backup: record
	tar czvf NuTrident_CompHEP_$(shell date -r $(shell ls -t $(backupFiles) | head -1) +'%y%m%d_%H%M%S').tgz $(backupFiles) record.log

# backup working directory
backupworking:
	tar czvf NuTrident_CompHEP_working_$(shell date -r $(shell ls -t $(myOwnFiles) | head -1) +'%y%m%d_%H%M%S').tgz $(myOwnFiles)

# scan nm_O16_mM_Zprime
scanNmO16mMZprime:
	. prepare.sh nm_O16_mM_Zprime.tgz Z 1
	./split.sh nm_O16_mM_Zprime 15.

# scan nm_H1_mM_Zprime
scanNmH1mMZprime:
	. prepare.sh nm_H1_mM_Zprime.tgz Z 1
	./split.sh nm_H1_mM_Zprime 0.938

# scan nt_O16_mM_Zprime
scanNtO16mMZprime:
	. prepare.sh nt_O16_mM_Zprime.tgz Z 0
	./split.sh nt_O16_mM_Zprime 15.

# scan nt_H1_mM_Zprime
scanNtH1mMZprime:
	. prepare.sh nt_H1_mM_Zprime.tgz Z 0
	./split.sh nt_H1_mM_Zprime 0.938

# scan nm_O16_mM_Sprime
scanNmO16mMSprime:
	. prepare.sh nm_O16_mM_Sprime.tgz S 1
	./split.sh nm_O16_mM_Sprime 15.

# scan nm_H1_mM_Sprime
scanNmH1mMSprime:
	. prepare.sh nm_H1_mM_Sprime.tgz S 1
	./split.sh nm_H1_mM_Sprime 0.938

# scan nt_O16_mM_Sprime
scanNtO16mMSprime:
	. prepare.sh nt_O16_mM_Sprime.tgz S 0
	./split.sh nt_O16_mM_Sprime 15.

# scan nt_H1_mM_Sprime
scanNtH1mMSprime:
	. prepare.sh nt_H1_mM_Sprime.tgz S 0
	./split.sh nt_H1_mM_Sprime 0.938
