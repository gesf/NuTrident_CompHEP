#!/bin/bash
#
# Bash script for running CompHEP for SM prediction of neutrino trident production cross section
#
# Usage: ./sm.sh fileName mass withCC
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Mar 14 13:56:48 CET 2017

fileName=$1
mass=$2
withCC=$3

. prepare.sh $fileName.tgz Z $withCC

cd $fileName
./run.sh $mass
cp xsec.dat ../$fileName"_xsec.dat"

cd -
