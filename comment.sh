#!/bin/bash
#
# Bash script for comment.sh
#
# This script is to comment the first 3 lines of the distribution tables produced by CompHEP
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Mar 14 11:28:45 CET 2017

for inFile in ${@:1}; do
	sed -e '1,3s/^/# /g' $inFile > temp.txt
	mv temp.txt $inFile
done
