#!/bin/bash
#
# Bash script kin.sh for adding kinematics pattern
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 15 09:56:16 CET 2017

# file name
fileName=$1

echo "modifying $fileName"

# ------------------ head line ---------------------
# search headline
begLine=$(grep -n "#Kinematical_scheme" $fileName)
# position of ":"
begPos=$(expr index $begLine ":")
# line number
begNum=$( echo "${begLine:0:begPos-1}+1" | bc -l)

# ------------------ end line ---------------------
# search endline
endLine=$(grep -n "\->" $fileName | tail -n 1)
# position of ":"
endPos=$(expr index "$endLine" ":")
# line number
endNum=${endLine:0:endPos-1}

# ------------------ replacement ---------------------
sed -e $begNum,$endNum"d" \
    -e "/#Kinematical_scheme/a 12 -> 345 , 6" \
    -e "/#Kinematical_scheme/a 345 -> 3 , 45" \
    -e "/#Kinematical_scheme/a 45 -> 4 , 5" \
    $fileName > $fileName.log
rm $fileName
mv $fileName.log $fileName
