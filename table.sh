#!/bin/bash
#
# Bash script table.sh for making tables
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 29 15:08:38 CEST 2017

# check input target mass
if [ $# -lt 5 ]; then
	echo "At least 5 arguments (fileName, nuclei mass, withCC, NP, and Enu) are needed!"
	exit
fi

# fileName
fileName=$1
# mass
mass=$2
# withCC
withCC=$3
# NP
sNP=$4
# Enu
Enu=$5

# prepare
. prepare.sh $fileName.tgz $sNP $withCC

# distributions
cd $fileName/results
cp ../../dist.sh .
./dist.sh batch.dat $Enu
./dist.sh session.dat $Enu
cd ..

# run
./run_Enu.sh $mass $Enu

# exit
cd ..
