#!/bin/bash
#
# Bash script copy.sh for
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 29 19:11:45 CEST 2017

# original table file
tabFile="working/results/tab_1.txt"

# check table
if [ -e $tabFile ]; then
	echo "copy $tabFile"
else
	echo "The file $tabFile doesn't exist!"
	exit
fi

# check inputs
if [ $# -lt 1 ]; then
	echo "At least one argument for file name is needed!"
	exit
fi

# file name
fileName=$1

# copy
cp $tabFile dist/$fileName
cd dist
../comment.sh $fileName
cd ..

# delete original table
rm $tabFile
