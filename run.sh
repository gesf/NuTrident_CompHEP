#!/bin/bash
#
# Bash script for run_SM.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Sep 23 11:54:17 CEST 2015
#
# This script is used to call the numerical batch mode of comphep to run simulation
# for nutrino trident production. Three arguments are needed:
#
# arg 1: the mass of the target nuclei [GeV]
# arg 2: contriol variable for split runs, see scan.sh.
# arg 3: the precision of xsec [percentage]. It will run extra simulation until the precision is good enough [default = 1%]
# arg 4: threshold of chi^2 value. It will run extra simulation [by removing statistics] until the chi^2 is below the threshold [default = 2.]
#
#

echo "run.sh begins at $(date)"

# Author info
author="Shao-Feng Ge"
email="gesf02@gmail.com"

# check input target mass
if [ $# -lt 1 ]; then
	echo "At least one argument for target mass is needed!"
	exit
fi

mOx=$1

# # of calls for each vegas simulation
nCall=100000

# auxiliary control variable
if [ $# -gt 1 ]; then
	xsecFile="xsec_"$(printf "\\$(printf '%03o' "$(echo "scale = 3; $2+97" | bc -l)")")".dat"
else
	xsecFile="xsec.dat"
fi

# precision
if [ $# -gt 2 ]; then
	precision=$3
else
	precision="1.0"
fi

# chi^2 threshold
if [ $# -gt 3 ]; then
	chi2threshold=$4
else
	chi2threshold="2."
fi

# prepare xsec file
if [ -e $xsecFile ]; then
	rm $xsecFile
fi
touch $xsecFile
# model parameters
./num_batch.pl -show params > $xsecFile
sed -i "s/^/# /g" $xsecFile
# signature
echo "# "$author "("$email")" >> $xsecFile
echo "# " >> $xsecFile
echo "# "$(date) >> $xsecFile
echo "# " >> $xsecFile
# columns names
echo "# E_nu [GeV] xsec [pb]        Error [%]  nCall    chi**2" >> $xsecFile

# main loop over E_nu [GeV]
for i in 1.0 3.0 5. 7.0 10. 30. 50. 70. 100. 300. 500. 700. 1000.
# for i in 600. 1000.
do
	# E_cm
	Ecm=$(echo "scale = 3; sqrt($mOx*$mOx+2.*$i*$mOx)" | bc -l)
	echo
	echo "---------------------------------------------"
	echo "E_nu = $i [GeV], E_cm = $Ecm [GeV]"
  # fresh start
	./num_batch.pl -run clean
	rm results/prt_*
	# replace E_cm # nCall
	sed -i "s/.*SQRT.*/  SQRT\(S\) $Ecm/g" results/batch.dat results/session.dat
	# precision (%)
	error="1.1"
	chi2=$chi2threshold
	nCall=100000
	sed -i "s/.*Vegas_calls*/#Vegas_calls $nCall""x5/g" results/batch.dat results/session.dat
	# loop if not good
	while [[ $(echo $error'>'$precision | bc -l) == "1" || $(echo $chi2'>'$chi2threshold | bc -l) == "1" || $xsec == "NAN" || $chi2 == "nan" ]]; do
		# delete statistics
 		if [[ $(echo $chi2'>'$chi2threshold | bc -l) == "1" || $xsec == "NAN" || $chi2 == "nan" ]]; then
			./num_batch.pl -run cleanstat
			nCall=$(echo "1.5*"$nCall | bc -l)
			if [[ "$nCall" == *'.'* ]]; then
				len=$(expr index "$nCall" ".")
				nCall=${nCall:0:len-1}
			fi
			echo "nCall = "$nCall
			./num_batch.pl -call $nCall
		fi
		# running
		./num_batch.pl -run vegas > vegas.log
		# extracting
		error=$(grep "< >" vegas.log | awk '{printf "%f", $4}')
		 chi2=$(grep "< >" vegas.log | awk '{printf "%f", $6}')
		 xsec=$(grep "< >" vegas.log | awk '{printf "%.6E", $3}')
		# print
		echo "xsec =" $xsec "[pb], error =" $error"% [precision =" $precision"%], chi^2 =" $chi2 "[threshold =" $chi2threshold"]"
	done
	# recording
	cat vegas.log | grep "< >" >> $xsecFile
	sed -i "s/ < >/$i      /g" $xsecFile
done

echo "run.sh ends at $(date)"
