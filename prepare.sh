#!/bin/bash
#
# Bash script for prepare.sh
#
# Usage: . prepare.sh fileName, NuTriObj, withCC
#
#        1) fileName - something like nm_O16_mM_Zprime.tgz
#        2) NuTriObj = Z or S
#        3) withCC = 1 or 0
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Sep 29 18:30:57 CEST 2015
#

if [ $# -lt 3 ]; then
	echo "At least three arguments for tgz filename, neutrino trident object name, and withCC are needed!"
else
	# file name
	x=$1
	filename=${x%.*}
	# uncompress
	tar xzvf $1
	cd $filename
  # configure & make
	./configure --with-m64
	make all
	# set up batch & session files
	./num_batch.pl
	cd results
	# kinematics
	cp ../../kin.sh .
	./kin.sh batch.dat
	./kin.sh session.dat
	# regularization
	cp ../../reg.sh .
	./reg.sh batch.dat $3
	./reg.sh session.dat $3
	# xsec folder
	cd ..
	mkdir xsec
	# run & scan scripts
	cp ../scan.sh .
	cp ../run.sh .
	cp ../run_Enu.sh .
	# exit
	cd ..
	export NuTriObj=$2
fi
