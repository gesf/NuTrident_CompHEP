/*
* Copyright (C) 2002-2009, CompHEP Collaboration
* ------------------------------------------------------
*/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "service2/include/4_vector.h"
#include "num/include/LesHouches.h"
#include "userFun.h"

double 
userFunction (char *name)
{
/*
    The following commented line is a example of use userFunction.
    It define user variable "UMtAv" that calculate in the process
    of MC integration. This variable can use in the cut-table.
    More details see in Manual :-)
    
*/
/*
  double pt3, pt4, pt5, pt6, qsc;

  pt3 = pvect[8 + 1] * pvect[8 + 1] + pvect[8 + 2] * pvect[8 + 2];
  pt4 = pvect[12 + 1] * pvect[12 + 1] + pvect[12 + 2] * pvect[12 + 2];
  pt5 = pvect[16 + 1] * pvect[16 + 1] + pvect[16 + 2] * pvect[16 + 2];
  pt6 = pvect[20 + 1] * pvect[20 + 1] + pvect[20 + 2] * pvect[20 + 2];
  qsc = sqrt ((175.0 * 175.0) + (pt3 + pt4 + pt5 + pt6) / 4.0);

  if (strcmp (name, "MtAv") == 0)
    {
      if (!finite (qsc))
	fprintf (stdout, " uvar= %18.15g\n", qsc);
      return qsc;
    }
*/

  fprintf (stdout, " ***** userFunction: not defined: %s\n", name);
  exit (10);
  return 0.;
}

/*
typedef struct eventUP
  {
    int NpartUP;                    // number of particle entries in this event
    int IDprocUP;                   // ID of the process for this event
    int IDpartUP[MAXpart];          // particle ID according to Particle Data Group convention
    int statusUP[MAXpart];          // status code:
                                    //   -1 Incoming particle
                                    //   +1 Outgoing final state particle
                                    //   -2 Intermediate space-like propagator defining an 
                                    //   $x$ and $Q^2$ which should be preserved
                                    //   +2 Intermediate resonance, Mass should be preserved
                                    //   +3 Intermediate resonance, for documentation only
                                    //   -9 Incoming beam particles at time $t=-\infty$

    int motherUP[2][MAXpart];       // index of first and last mother
    int colorflowUP[2][MAXpart];    // color flow
                                    //   ICOLUP(1,I) - integer tag for the color flow 
                                    //   line passing through the color of the particle
                                    //   ICOLUP(2,I) - integer tag for the color flow 
                                    //   line passing through the anti-color of the particle

    double XweightUP;               // event weight
    double QscaleUP;                // scale of the event in GeV, as used for calculation of  PDFs
    double QEDalphaUP;              // the QED coupling used for this event
    double QCDalphaUP;              // the QCD coupling used for this event
    double momentumUP[5][MAXpart];  // lab frame momentum $(P_x, P_y, P_z, E, M)$ of particle in GeV
    double timelifeUP[MAXpart];     // invariant lifetime $c\tau$ (distance from 
                                    // production to decay) in mm
    double spinUP[MAXpart];         // cosine of the angle between the spin-vector of 
                                    // particle I and the 3-momentum of the decaying 
                                    // particle, specified in the lab frame
  }
*/

static double get_llprod (double * p, double * q) {
  return p[0] * q[0] - p[1] * q[1] - p[2] * q[2] - p[3] * q[3];
}

double 
cutFunction (eventUP * ev)
{
/*
An example:
All leptons are selected. If any invariant mass (l1,l2) 
in the event is less than 10. GeV, the event is rejected

Structure of the eventUP structure is given above
*/
  int i, j;
  int nlep = 0;
  int nmass = 0;
  double selection = 1.;
  double leppx[16];
  double leppy[16];
  double leppz[16];
  double lepp0[16];
  double leppm[16];
  double llmass[16] = {0.};

  for (i = 0; i < ev->NpartUP; ++i) {
    int kf = abs (ev->IDpartUP[i]);
    if (11 == kf || 13 == kf || 15 == kf) {
      leppx[nlep] = ev->momentumUP[0][i];
      leppy[nlep] = ev->momentumUP[1][i];
      leppz[nlep] = ev->momentumUP[2][i];
      lepp0[nlep] = ev->momentumUP[3][i];
      leppm[nlep] = ev->momentumUP[4][i];
      ++nlep;
    }
  }

  for (i = 0; i < nlep; ++i) {
    double p1[4] = {lepp0[i], leppx[i], leppy[i], leppz[i]};
    double ms1 = leppm[i] * leppm[i];
    for (j = i + 1; j < nlep; ++j) {
      double p2[4] = {lepp0[j], leppx[j], leppy[j], leppz[j]};
      double ms2 = leppm[j] * leppm[j];
      llmass [nmass] = sqrt (2. * get_llprod (p1, p2) + ms1 + ms2);
      ++nmass;
    }
  }

  for (i = 0; i < nmass; ++i) {
    if (llmass[i] < 10.)
      selection = 0.;
  }

  return selection;
}


/*   
copied from utile/usrFF.c of CalcHEP (Shao-Feng Ge, Mon Mar 13 10:40:04 CET 2017)

The usrFF function appears as factor at squared matrix element for 
Monte Carlo calculations.  CalcHEP code has a dummy version of this function 
which always return 1.  The dummy version is replaced on the  user one if 
its code is passed to CalcHEP linker via 'Libraries' model file. One can use 
CALCHEP and  WORK  environment variables  to specify path to the code. 
These variables are  automatically defined in calchep and calchep_batch  scripts. 
Also one can use any other environment variables defined separately. 
 Parameters of usrFF:
    nIn - number of incoming particles;
    nOut- number of outgoing particles;
    pvect  presents momenta of particles: 
       4-momentum of  i^{th} particle ( i=0,1,...,nIn+nOut-1)  is 
       q[k]=pvect[4*i+k]  k=0,1,2,3; 
       q[0] - in particle energy, which always is positive.
       q[3] - specify projection of momentum on axis of collision.

    pName[i] (i=0,..nIn+nOut-1) contains name of i^th particle involved in
    reaction and pCode[i] is the corresponding PDG code. 

    Auxiliary functions which can help for construct usrFF are 
*/

// momentum transfer
double q2(double *pvect, int i, int j) // q2 < 0
{
	double *Pi = pvect + 4*i;
	double *Pj = pvect + 4*j;
	double q[4];
	q[0] = Pi[0] - Pj[0];
	q[1] = Pi[1] - Pj[1];
	q[2] = Pi[2] - Pj[2];
	q[3] = Pi[3] - Pj[3];

	double Q2 = q[0]*q[0] - q[1]*q[1] - q[2]*q[2] - q[3]*q[3];
//	printf("q = (%f, %f, %f, %f) [Q2 = %f]\n", q[0], q[1], q[2], q[3], Q2);

	return Q2;
}

// particle mass
double pMass(double *pvect, int i)
{
	double *Pi = pvect + 4*i;
	double pMass2 = Pi[0]*Pi[0] - Pi[1]*Pi[1] - Pi[2]*Pi[2] - Pi[3]*Pi[3];
	return sqrt(pMass2);
}

double a(double A) // GeV^{-1}
{
	return 5.076*(0.58 + 0.82*pow(A, 1./3.));
}

// Phys.Rev. D3 (1971) 2686-2706
double FI(double Q2, double A)
{
	return 1./pow(1.+pow(a(A),2.)*Q2/12., 2.);
}

// Phys.Rev. D3 (1971) 2686-2706
double FII(double Q2, double A)
{
	return exp(-pow(a(A),2.)*Q2/6.);
}

// Phys.Rev. D3 (1971) 2686-2706
double RA(double A)
{
	return pow(A,1./3.)/0.217;
}

// 1612.05642
double Gdip(double Q2)
{
	return 1./pow(1.+Q2/0.71, 2.);
}

// 1612.05642
double TAUdip(double Q2)
{
	return Q2/4.; // M = (mp+mn)/2 ~ 1GeV
}

// 1612.05642
double Fdip(double Q2, double Z)
{
	double tau = TAUdip(Q2);
	double xi = 4.7;

	return Gdip(Q2)*(1.+tau*xi)/(1.+tau)/sqrt(Z); // one Z already in vertex
}

double factor_usrFF = 1.;
double usrFF(int nIn, int nOut,  double * pvect)
{
	/*
	for (int iP = 0; iP < nIn+nOut; iP++)
		printf("%i: (%f, %f, %f, %f), mass = %f\n", iP, pvect[4*iP+0], pvect[4*iP+1], pvect[4*iP+2], pvect[4*iP+3], pMass(pvect, iP));
	*/

	// target mass
	double mN1 = pMass(pvect, 1);
	double mN2 = pMass(pvect, nIn+nOut-1);
	//printf("mN1 = %f, mN2 = %f\n", mN1, mN2);
	// A & Z
	double pA, pZ;
	// target type [0: no form factor, 1: O16, 2: H1]
	int pType = 0;
	// O16
	if (fabs(mN1-15.) < 0.001 && fabs(mN2-15.) < 0.001)
	{
		pType = 1;
		pA    = 16.;
		pZ    = 8.;
	}
	// H1
	else if (fabs(mN1-0.938) < 0.001 && fabs(mN2-0.938) < 0.001)
	{
		pType = 2;
		pA    = 1.;
		pZ    = 1.;
	}

	// momentum transfer
	double Q2 = -q2(pvect, 1, nIn+nOut-1); // Q2 > 0
	//printf("Q2 = %f\n\n", Q2);

	factor_usrFF = 1.;
	if (pType == 0)
	{
		if (Q2 < 1.)
			factor_usrFF = 0.;
	}
	else
	{
		if (Q2 > 1.)
			factor_usrFF = 0.;
		else
			factor_usrFF = Q2*RA(pA) < 1. ? FI(Q2,pA) : Fdip(Q2, pZ);
	}

	//printf("Q2 = %f, form factor = %f\n", Q2, factor_usrFF);
	return pow(factor_usrFF, 2.);
}
