/*
* Copyright (C) 2001-2009, CompHEP Collaboration
* ------------------------------------------------------
*/
#ifndef __USR_PARAM__
#define __USR_PARAM__

extern double userFunction (char *param);
extern double cutFunction (eventUP * ev);

// momentum transfer
double q2(double *pvect, int i, int j);
double pMass(double *pvect, int i);
// size
double a(double A); // GeV^{-1}
double RA(double A); // GeV^{-1}
// coherent
double FI(double Q2, double A); // Q2 = -q^2 > 0
double FII(double Q2, double A);
// diffractive
double Gdip(double Q2);
double TAUdip(double Q2);
double Fdip(double Q2, double Z);

extern double factor_usrFF;
extern double usrFF(int nIn, int nOut,  double * pvect);
/*   
copied from utile/usrFF.c of CalcHEP (Shao-Feng Ge, Mon Mar 13 10:40:04 CET 2017)

The usrFF function appears as factor at squared matrix element for 
Monte Carlo calculations.  CalcHEP code has a dummy version of this function 
which always return 1.  The dummy version is replaced on the  user one if 
its code is passed to CalcHEP linker via 'Libraries' model file. One can use 
CALCHEP and  WORK  environment variables  to specify path to the code. 
These variables are  automatically defined in calchep and calchep_batch  scripts. 
Also one can use any other environment variables defined separately. 
 Parameters of usrFF:
    nIn - number of incoming particles;
    nOut- number of outgoing particles;
    pvect  presents momenta of particles: 
       4-momentum of  i^{th} particle ( i=0,1,...,nIn+nOut-1)  is 
       q[k]=pvect[4*i+k]  k=0,1,2,3; 
       q[0] - in particle energy, which always is positive.
       q[3] - specify projection of momentum on axis of collision.

    pName[i] (i=0,..nIn+nOut-1) contains name of i^th particle involved in
    reaction and pCode[i] is the corresponding PDG code. 

    Auxiliary functions which can help for construct usrFF are 
*/


#endif
