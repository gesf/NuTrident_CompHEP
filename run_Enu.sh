#!/bin/bash
#
# Bash script run_Enu.sh for running neutrino trident production with fixed E_cm
#
# Usage: ./run_Enu.sh mNucl Enu
#
#        mNucl = the nuclei mass
#          Enu = the neutrino energy
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 29 14:28:03 CEST 2017

# check input target mass
if [ $# -lt 2 ]; then
	echo "At least two arguments (mN & Enu) are needed!"
	exit
fi

# nuclei mass
mNucl=$1
# E_nu
Enu=$2
# E_cm
Ecm=$(echo "scale = 3; sqrt($mNucl*$mNucl+2.*$Enu*$mNucl)" | bc -l)

# of calls for each vegas simulation
nCall=100000

# precision
if [ $# -gt 2 ]; then
	precision=$3
else
	precision="1.0"
fi

# chi^2 threshold
if [ $# -gt 3 ]; then
	chi2threshold=$4
else
	chi2threshold="2."
fi

echo "E_nu = $Enu [GeV], E_cm = $Ecm [GeV]"
# fresh start
./num_batch.pl -run clean
rm results/prt_*
# replace E_cm # nCall
sed -i "s/.*SQRT.*/  SQRT\(S\) $Ecm/g" results/batch.dat results/session.dat
sed -i "s/.*Vegas_calls*/#Vegas_calls $nCall""x5/g" results/batch.dat results/session.dat
# precision (%)
error="1.1"
chi2=$chi2threshold
# loop if not good
while [[ $(echo $error'>'$precision | bc -l) == "1" || $(echo $chi2'>'$chi2threshold | bc -l) == "1" || $xsec == "NAN" || $chi2 == "nan" ]]; do
	# delete statistics
 		if [[ $(echo $chi2'>'$chi2threshold | bc -l) == "1" || $xsec == "NAN" || $chi2 == "nan" ]]; then
		./num_batch.pl -run cleanstat
		nCall=$(echo "1.5*"$nCall | bc -l)
		if [[ "$nCall" == *'.'* ]]; then
			len=$(expr index "$nCall" ".")
			nCall=${nCall:0:len-1}
		fi
		echo "nCall = "$nCall
		./num_batch.pl -call $nCall
	fi
	# running
	./num_batch.pl -run vegas > vegas.log
	# extracting
	error=$(grep "< >" vegas.log | awk '{printf "%f", $4}')
	 chi2=$(grep "< >" vegas.log | awk '{printf "%f", $6}')
	 xsec=$(grep "< >" vegas.log | awk '{printf "%.6E", $3}')
	# print
	echo "xsec =" $xsec "[pb], error =" $error"% [precision =" $precision"%], chi^2 =" $chi2 "[threshold =" $chi2threshold"]"
done
