#!/bin/bash
#
# Bash script wait.sh for waiting to run splitted scan
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 15 16:19:12 CET 2017

# CPU number
nCPU=$(cat /proc/cpuinfo | awk '/^processor/{print $3}' | tail -n 1)

# process (scan.sh) number
nProcess=$(echo $nCPU+1 | bc -l)

# wait
while [[ $nProcess -gt $nCPU ]]; do
	nProcess=$(ps aux | grep "scan\.sh" | wc -l)
	sleep 1m
done
