#!/bin/bash
#
# Bash script for count.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Sep 30 10:36:55 CEST 2015
#
# This script counts the # of prt files in split folders.
#

# check argument
if [ $# -lt 1 ]; then
	echo "The parent folder name is needed!"
	exit
fi

# loop
for i in {0..7}; do
	cd $1"_"$i"/results"
	num1=$(ls -l prt_* | wc -l)
	num2=$(grep "\[pb\]" prt_* | wc -l)
	cd ..
	if [ -e xsec*.dat ]; then
		stat="pending"
	else
		stat="finished"
	fi
	echo $1"_"$i":" $num1 "($num2)" $stat
	cd ..
done
