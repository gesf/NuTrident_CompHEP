#!/bin/bash
#
# Bash script for kill.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Oct  6 11:49:04 CEST 2015
#

pkill scan.sh
pkill run.sh
pkill num_batch.pl
pkill n_comphep.exe
