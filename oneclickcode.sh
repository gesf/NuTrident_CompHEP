#!/bin/bash
#
# Bash script for oneclickcode.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Mar 14 11:42:24 CET 2017

target=$1

cp working/oneclickcode.tar.bz2 .
tar xjvf oneclickcode.tar.bz2
mv oneclickcode $target
mv $target/usercode/userFun.c $target/usercode/userFun_old.c
mv $target/usercode/userFun.h $target/usercode/userFun_old.h
mv $target/chepcode/usr/userFun.c $target/chepcode/usr/userFun_old.c
mv $target/chepcode/usr/userFun.h $target/chepcode/usr/userFun_old.h
cp -iav working/usr/userFun.[ch] $target/usercode
cp -iav working/usr/userFun.[ch] $target/chepcode/usr
tar czvf $target".tgz" $target
rm -rf $target
rm -f oneclickcode.tar.bz2
