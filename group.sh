#!/bin/bash
#
# Bash script for group.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Sep 29 14:02:19 CEST 2015
#
# This script groups the xsec obtained by split running 
# and compress them into an archive file. One argument is needed:
#
# argument: parent folder name
#

# check # of arguments
if [ $# -lt 1 ]; then
	echo "The parent folder name is needed!"
	exit
fi

# create container
if [ -d $1"_xsec" ]; then
	rm -rf $1"_xsec"
fi
mkdir $1"_xsec"

# copy xsec files
for i in {0..7}
do
	if [ -d $1"_"$i ]; then
		cp $1"_"$i"/xsec/"* $1"_xsec"
		cp $1"_"$i"/scan.log" $1"_xsec/scan"_"`date +'%y%m%d_%H%M%S'`_"$i".log"
	else
		echo "The folder" $1"_"$i "doesn't exist!"
	fi
done

# archive
uname -a > record.log
echo >> record.log
echo $(pwd) >> record.log
echo >> record.log
date >> record.log
mv record.log $1"_xsec"
tar czvf $1"_xsec_"`date +'%y%m%d_%H%M%S'`.tgz $1"_xsec"
