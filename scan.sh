#!/bin/bash
#
# Bash script for scan.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Sep 28 18:08:17 CEST 2015
#
# This script sets the Z_prime/S_prime mass & coupling. Then it calls run.sh
# to run the numerical batch mode of CompHEP for simulation. Two arguments
# are needed:
#
# arg 1: the mass of the target nucleus [GeV]
# arg 2: the control variable for split mode [0..7]
# arg 3: replace (default = 0) or append (1)
# arg 4: the control variable for final index [used together with arg 2]
#

# check # of arguments
if [ $# -lt 1 ]; then
	echo "At least one argument for target mass is needed!"
	exit
fi

# mass
minMass=0.001
maxMass=1000.
nMass=24

# coupling
zG=0.6
minG=0.0001
maxG=1.
nG=20

# loop over mass
# control variable
if [ $# -gt 1 ]; then
	# double control variable
	if [ $# -gt 3 ]; then
		minIdx=$2
		maxIdx=$4
	# single control variable
	else
		minIdx=$[$2*3]
		maxIdx=$[$2*3+2]
		# final point also included
		if [ $2 -eq 7 ]; then
			maxIdx=24
		fi
	fi
# no control variable
else
	minIdx=0
	maxIdx=24
fi

# loop over mass
for i in $(seq $minIdx $maxIdx)
do
	# calculate mass
	mass=$(echo "scale = 6; e(l($minMass) + $i*(l($maxMass) - l($minMass))/$nMass)" | bc -l | awk '{printf "%f", $0}')
  # replacing
	sed -i "s/.*m"$NuTriObj"p.*/       m"$NuTriObj"p = $(echo $mass | awk '{printf "%.15E", $0}')/g" results/batch.dat results/session.dat

  # loop over coupling
	for j in {0..20}
	do
		# calculate coupling
		g=$(echo "scale = 7; e(l($minG) + $j*(l($maxG) - l($minG))/$nG)" | bc -l | awk '{printf "%f", $0}')

		# printing out
		echo 
		echo 
		echo "#######################################################################"
		echo "i =" $i", j =" $j
		echo "mass =" $(echo $mass | awk '{printf "%.15E", $0}')  "[GeV], g =" $(echo $g | awk '{printf "%.15E", $0}')
		echo $(date)
		
    # replacing
		sed -i "s/.*c"$NuTriObj"p.*/       c"$NuTriObj"p = $(echo $g    | awk '{printf "%.15E", $0}')/g" results/batch.dat results/session.dat

    # actually running simulation
		if [ $# -eq 2 ]; then
    	./run.sh $1 $2
		else
    	./run.sh $1
		fi

		# original file
		if [ $# -eq 2 ]; then
			file1=xsec_"$(printf "\\$(printf '%03o' "$(echo "scale = 3; $2+97" | bc -l)")")".dat 
		else
			file1=xsec.dat
		fi

		# destination file
    dest=xsec/xsec_$(printf "\\$(printf '%03o' "$(echo "scale = 3; $i+97" | bc -l)")")$(printf "\\$(printf '%03o' "$(echo "scale = 3; $j+97" | bc -l)")").dat

		# archieve result
		if [ $3 == "1" ]; then # append
			cat $file1 >> $dest
		else # replace
			cp $file1 $dest
		fi

		# removal
		rm $file1
	done
done

# email notification
folderName=${PWD##*/}
echo "The session in $(pwd) has finished at $(date)" | mail -s "$folderName @ $HOSTNAME finished" gesf02@gmail.com
