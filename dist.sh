#!/bin/bash
#
# Bash script dist.sh for defining distributions to make plot
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 29 14:43:21 

# check input target mass
if [ $# -lt 2 ]; then
	echo "At least two arguments (fileName & Enu) are needed!"
	exit
fi

# file name
fileName=$1

# Enu
Enu=$2
sEnu=$(printf "%.3f" $Enu)
len=$(echo "13-${#sEnu}" | bc -l)

for i in `seq 1 $len`; do
	sEnu=$sEnu' '
done

echo "modifying $fileName"

# ------------------ replacement ---------------------
sed -e "/Rest Frame/a A45          |0            |30           |              |" \
    -e "/Rest Frame/a E4           |0            |$sEnu|              |" \
    -e "/Rest Frame/a E5           |0            |$sEnu|              |" \
    -e "/Rest Frame/a M45          |0            |5            |              |" \
    $fileName > $fileName.log

rm $fileName
mv $fileName.log $fileName
