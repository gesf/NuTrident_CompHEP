#!/bin/bash
#
# Bash script for batch.sh
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Tue Sep 29 13:37:16 CEST 2015
#
# This code splits the existing folder prepared in advance and
# enters each copy to call scan.sh for splitted simulation
#
# arg 1 = [parent] folder name
# arg 2 = mass of target nucleus
# arg 3 = replace (0) or append (default = 1)

# check arguments
if [ $# -lt 1 ]; then
	echo "The 1st argument for folder name is needed!"
	exit
fi

if [ $# -lt 2 ]; then
	echo "The 2nd argument for target mass is needed!"
	exit
fi

# split
for i in {0..7}
do
	# wait
	./wait.sh
	# create individual folder
	if [ ! -d $1"_"$i ]; then
		cp -a $1 $1"_"$i
	fi

	# change working directory
	cd $1"_"$i

	# execute
	if [ $# -lt 3 ]; then
		nohup ./scan.sh $2 $i 1 > scan.log &
	else
		nohup ./scan.sh $2 $i $3 > scan.log &
	fi

	# change back
	cd ..
done
