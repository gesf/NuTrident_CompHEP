#!/bin/bash
#
# Bash script reg.sh for adding propagator regularization
#
# Usage: ./reg.sh fileName withCC
#
#        1) fileName = batch.dat or session.dat
#        2) withCC = 1 or 0
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 15 10:48:13 CET 2017

# file name
fileName=$1
withCC=$2

echo "modifying $fileName"

# ------------------ replacement ---------------------
if [ $withCC == "1" ]; then
sed -e "/Momentum/a 26           |0        |0        |2     |" \
    -e "/Momentum/a 13           |MZ       |2.44     |2     |" \
    -e "/Momentum/a 14           |MW       |2.03     |2     |" \
    -e "/Momentum/a 35           |MW       |2.03     |2     |" \
    -e "/Momentum/a 246          |Mm       |0        |2     |" \
    -e "/Momentum/a 256          |Mm       |0        |2     |" \
    $fileName > $fileName.log
else
sed -e "/Momentum/a 26           |0        |0        |2     |" \
    -e "/Momentum/a 13           |MZ       |2.44     |2     |" \
    -e "/Momentum/a 246          |Mm       |0        |2     |" \
    -e "/Momentum/a 256          |Mm       |0        |2     |" \
    $fileName > $fileName.log
fi

rm $fileName
mv $fileName.log $fileName
