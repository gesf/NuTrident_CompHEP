#!/bin/bash
#
# Bash script Zprime.sh for
#
# Shao-Feng Ge (gesf02@gmail.com) 
# 
# created on Wed Mar 15 16:38:40 CET 2017

# nm_O16_mM
# . prepare.sh nm_O16_mM_Zprime.tgz Z 1
# ./split.sh nm_O16_mM_Zprime 15.

# nm_H1_mM
. prepare.sh nm_H1_mM_Zprime.tgz Z 1
./split.sh nm_H1_mM_Zprime 0.938

# nt_O16_mM
. prepare.sh nt_O16_mM_Zprime.tgz Z 0
./split.sh nt_O16_mM_Zprime 15.

# nt_H1_mM
. prepare.sh nt_H1_mM_Zprime.tgz Z 0
./split.sh nt_H1_mM_Zprime 0.938
